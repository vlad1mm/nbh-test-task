<?php

use App\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->fill([
            'role_id' => Role::first()->id,
            'email' => 'admin@test.com',
            'password' => bcrypt('secret'),
        ])->save();
    }
}
