<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRole(['name' => 'Administrator']);
        $this->createRole(['name' => 'User']);
    }

    protected function createRole($attributes)
    {
        $role = new \App\Role();
        $role->fill($attributes)->save();
    }
}
