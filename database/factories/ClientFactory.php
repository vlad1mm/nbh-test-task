<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->lastName,
        'birth_date' => $faker->date(),
        'phone_number' => $faker->phoneNumber,
        'password' => bcrypt('secret'),
        'email' => $faker->email,
        'address' => $faker->address,
        'country' => $faker->country,
        'trading_account_number' => $faker->bankAccountNumber,
        'balance' => $faker->numberBetween(-10000, 10000),
        'open_trades' => $faker->numberBetween(0, 100),
        'close_trades' => $faker->numberBetween(0, 100),
    ];
});