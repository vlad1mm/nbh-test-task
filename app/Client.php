<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Client
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $birth_date
 * @property string $phone_number
 * @property string $address
 * @property string $country
 * @property string $trading_account_number
 * @property int $balance
 * @property int $open_trades
 * @property int $close_trades
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereCloseTrades($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereOpenTrades($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereTradingAccountNumber($value)
 */
class Client extends Authenticatable
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var bool
     */
    protected $rememberTokenName = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'phone_number', 'birth_date', 'password',
        'address', 'country', 'trading_account_number', 'balance', 'open_trades', 'close_trades',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
