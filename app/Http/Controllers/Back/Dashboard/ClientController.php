<?php

namespace App\Http\Controllers\Back\Dashboard;

use App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ClientController extends Controller
{
    /**
     * ClientController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:back');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {
        $client = Client::findOrFail($id);
        return view('back/dashboard/client/index', compact('client'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $id)
    {
        $client = Client::findOrFail($id);
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('clients')->ignore($client->id)],
            'phone_number' => ['required', 'string', 'max:255'],
            'birth_date' => ['required', 'date', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'trading_account_number' => ['required', 'string', 'max:255'],
            'balance' => ['required', 'numeric'],
            'open_trades' => ['required', 'numeric', 'min:0'],
            'close_trades' => ['required', 'numeric', 'min:0'],
        ]);
        $client->fill($data)->save();

        \Session::flash('successMessage', 'Client successfully updated');

        return redirect()->route('back.dashboard.client', ['id' => $id]);
    }
}
