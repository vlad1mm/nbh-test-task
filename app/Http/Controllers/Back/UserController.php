<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUser;
use App\Role;
use App\User;

class UserController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:back');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::query()->get();
        return view('back/user/create', compact('roles'));
    }

    /**
     * @param CreateUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateUser $request)
    {
        $data = $request->validated();
        $data['password'] = bcrypt($data['password']);
        User::create($data);

        \Session::flash('successMessage', 'User successfully created');

        return redirect()->route('back.create-user');
    }
}
