<?php

namespace App\Http\Controllers\Back;

use App\Client;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:back');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $clients = Client::query()->get();
        return view('back/dashboard/index', compact('clients'));
    }
}
