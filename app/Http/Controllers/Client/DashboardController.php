<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUser;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:client');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $roles = Role::query()->get();
        return view('client/dashboard/index', compact('roles'));
    }

    /**
     * @param CreateUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createUser(CreateUser $request)
    {
        $data = $request->validated();
        $data['password'] = bcrypt($data['password']);
        User::create($data);

        \Session::flash('successMessage', 'User successfully created');

        return redirect()->route('client.dashboard');
    }
}
