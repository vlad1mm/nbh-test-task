<?php

namespace App\Http\Controllers\Client;

use App\Client;
use App\Http\Controllers\Controller;
use Faker\Factory;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:client');
    }

    /**
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard|mixed
     */
    protected function guard()
    {
        return Auth::guard('client');
    }

    /**
     * @return string
     */
    protected function redirectTo()
    {
        return route('client.dashboard');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:clients'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone_number' => ['required', 'string', 'max:255'],
            'birth_date' => ['required', 'date', 'max:255'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Client
     */
    protected function create(array $data)
    {
        $faker = Factory::create();

        $model = Client::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'birth_date' => $data['birth_date'],
            'phone_number' => $data['phone_number'],
            'password' => Hash::make($data['password']),
            'email' => $data['email'],
            'address' => $faker->address,
            'country' => $faker->country,
            'trading_account_number' => $faker->bankAccountNumber,
            'balance' => $faker->numberBetween(-10000, 10000),
            'open_trades' => $faker->numberBetween(0, 100),
            'close_trades' => $faker->numberBetween(0, 100),
        ]);

        Auth::guard('client')->login($model);

        \Session::flash('successMessage', 'Client successfully registered');

        return $model;
    }
}
