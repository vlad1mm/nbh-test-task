@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Client portal</div>

                <div class="card-body">
                    <ul>
                        @guest('client')
                            <li>
                                <a href="{{ route('client.login') }}">/login</a>
                            </li>
                            <li>
                                <a href="{{ route('client.register') }}">/register</a>
                            </li>
                        @else
                            <li>
                                <a href="{{ route('client.dashboard') }}">/dashboard</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Back office</div>

                <div class="card-body">
                    <ul>
                        @guest('back')
                            <li>
                                <a href="{{ route('back.login') }}">/login</a>
                            </li>
                        @else
                            <li>
                                <a href="{{ route('back.dashboard') }}">/dashboard</a>
                            </li>
                            <li>
                                <a href="{{ route('back.create-user') }}">/create-user</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
