@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Client portal dashboard') }}</div>

                <div class="card-body">
                    @if (Session::has('successMessage'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{ Session::get('successMessage') }}
                        </div>
                    @endif

                    @include('partials.user-form')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
