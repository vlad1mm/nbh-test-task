@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Back office dashboard') }}</div>

                <div class="card-body">
                    @if($clients->isEmpty())
                        <div class="alert alert-info">There are no registered clients</div>
                    @else
                        <h4>Clients list</h4>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <td>Name</td>
                                <td>E-mail</td>
                                <td>Date of Birth</td>
                                <td>Phone number</td>
                                <td>Trading account number</td>
                                <td>Balance</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $client)
                                <tr>
                                    <td><a href="{{ route('back.dashboard.client', ['id' => $client['id']]) }}">{{ $client['name'] }}</a></td>
                                    <td>{{ $client['email'] }}</td>
                                    <td>{{ $client['birth_date'] }}</td>
                                    <td>{{ $client['phone_number'] }}</td>
                                    <td>{{ $client['trading_account_number'] }}</td>
                                    <td>{{ $client['balance'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
