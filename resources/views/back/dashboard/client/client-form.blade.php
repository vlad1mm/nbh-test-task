@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="POST" action="">
    @csrf

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $client['name'] }}" required autocomplete="name" autofocus>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>

        <div class="col-md-6">
            <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ $client['surname'] }}" required autocomplete="surname" autofocus>

            @error('surname')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

        <div class="col-md-6">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $client['email'] }}" required autocomplete="email">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone number') }}</label>

        <div class="col-md-6">
            <input id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ $client['phone_number'] }}" required autocomplete="phone_number" autofocus>

            @error('phone_number')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="birth_date" class="col-md-4 col-form-label text-md-right">{{ __('Birth date') }}</label>

        <div class="col-md-6">
            <input id="birth_date" type="date" class="form-control @error('birth_date') is-invalid @enderror" name="birth_date" value="{{ $client['birth_date'] }}" required autocomplete="birth_date" autofocus>

            @error('birth_date')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

        <div class="col-md-6">
            <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ $client['country'] }}" required autocomplete="country" autofocus>

            @error('country')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

        <div class="col-md-6">
            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ $client['address'] }}" required autocomplete="address" autofocus>

            @error('address')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="trading_account_number" class="col-md-4 col-form-label text-md-right">{{ __('Trading account number') }}</label>

        <div class="col-md-6">
            <input id="trading_account_number" type="text" class="form-control @error('trading_account_number') is-invalid @enderror" name="trading_account_number" value="{{ $client['trading_account_number'] }}" required autocomplete="trading_account_number" autofocus>

            @error('trading_account_number')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="balance" class="col-md-4 col-form-label text-md-right">{{ __('Balance') }}</label>

        <div class="col-md-6">
            <input id="balance" type="number" class="form-control @error('balance') is-invalid @enderror" name="balance" value="{{ $client['balance'] }}" required autocomplete="balance" autofocus>

            @error('balance')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="open_trades" class="col-md-4 col-form-label text-md-right">{{ __('Open trades') }}</label>

        <div class="col-md-6">
            <input id="open_trades" type="number" min="0" class="form-control @error('open_trades') is-invalid @enderror" name="open_trades" value="{{ $client['open_trades'] }}" required autocomplete="open_trades" autofocus>

            @error('open_trades')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="close_trades" class="col-md-4 col-form-label text-md-right">{{ __('Close trades') }}</label>

        <div class="col-md-6">
            <input id="close_trades" type="number" min="0" class="form-control @error('close_trades') is-invalid @enderror" name="close_trades" value="{{ $client['close_trades'] }}" required autocomplete="close_trades" autofocus>

            @error('close_trades')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('Edit') }}
            </button>
        </div>
    </div>
</form>