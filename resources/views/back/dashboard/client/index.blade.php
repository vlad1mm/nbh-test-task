@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Back office dashboard') }}</div>

                    <div class="card-body">

                        @if (Session::has('successMessage'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                {{ Session::get('successMessage') }}
                            </div>
                        @endif

                        <dl class="dl-horizontal">
                            <dt>Name</dt>
                            <dd>{{ $client['name'] }}</dd>

                            <dt>Surname</dt>
                            <dd>{{ $client['surname'] }}</dd>

                            <dt>E-mail</dt>
                            <dd>{{ $client['email'] }}</dd>

                            <dt>Phone number</dt>
                            <dd>{{ $client['phone_number'] }}</dd>

                            <dt>Date of Birth</dt>
                            <dd>{{ $client['birth_date'] }}</dd>

                            <dt>Country</dt>
                            <dd>{{ $client['country'] }}</dd>

                            <dt>Address</dt>
                            <dd>{{ $client['address'] }}</dd>

                            <dt>Trading account number</dt>
                            <dd>{{ $client['trading_account_number'] }}</dd>

                            <dt>Balance</dt>
                            <dd>{{ $client['balance'] }}</dd>

                            <dt>Open trades</dt>
                            <dd>{{ $client['open_trades'] }}</dd>

                            <dt>Close trades</dt>
                            <dd>{{ $client['close_trades'] }}</dd>
                        </dl>

                        @if(Auth::guard('back')->user()['role']['name'] == 'Administrator')
                            @include('back.dashboard.client.client-form')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
