<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'reset' => false,
]);

Route::get('/', 'HomeController@index')->name('home');

Route::prefix('/client')->name('client.')->namespace('Client')->group(function () {
    Route::get('/', function () {
        return redirect()->route('home');
    })->name('home');

    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login');

    Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'RegisterController@register');

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('/dashboard', 'DashboardController@createUser');
});

Route::prefix('/back')->name('back.')->namespace('Back')->group(function () {
    Route::get('/', function () {
        return redirect()->route('home');
    })->name('home');

    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login');

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/client/{id}', 'Dashboard\ClientController@index')->name('dashboard.client');
    Route::post('/dashboard/client/{id}', 'Dashboard\ClientController@store');

    Route::get('/create-user', 'UserController@create')->name('create-user');
    Route::post('/create-user', 'UserController@store');
});
