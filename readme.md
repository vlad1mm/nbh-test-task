# nbh-test-task

## Installation

```
$ composer install
$ cp .env.example .env
$ php artisan migrate --seed
$ php artisan serve
```

Back-office user credentials: admin@test.com / secret

## Testing

`$ php vendor/phpunit/phpunit/phpunit`
