<?php

namespace Tests\Unit;

use App\Client;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AppTest extends TestCase
{
    use WithFaker;

    /**
     * @return void
     */
    public function testUserCreationFromClient()
    {
        $client = factory(Client::class)->make();
        $route = route('client.dashboard');

        $response = $this
            ->actingAs($client, 'client')
            ->post($route, $this->generateUserData());

        $response
            ->assertRedirect($route);
    }

    /**
     * @return void
     */
    public function testUserCreationFromBack()
    {
        $user = factory(User::class)->make();
        $route = route('back.create-user');

        $response = $this
            ->actingAs($user, 'back')
            ->post($route, $this->generateUserData());

        $response
            ->assertRedirect($route);
    }

    /**
     * @return void
     */
    public function testClientRegistration()
    {
        $route = route('client.register');
        $clientData = $this->generateClientData();
        $clientData['password_confirmation'] = $clientData['password'];

        $response = $this
            ->post($route, $clientData);

        $response
            ->assertRedirect(route('client.dashboard'));
    }

    /**
     * @return void
     */
    public function testClientUpdate()
    {
        $user = factory(User::class)->make();
        $client = Client::first();

        // preserve client credentials
        $clientData = $this->generateClientData();
        $clientData['email'] = $client['email'];
        $clientData['password'] = $client['password'];

        $route = route('back.dashboard.client', ['id' => $client['id']]);

        $response = $this
            ->actingAs($user, 'back')
            ->post($route, $clientData);

        $response
            ->assertRedirect($route);
    }

    /**
     * @return void
     */
    public function testClientsList()
    {
        $user = factory(User::class)->make();
        $route = route('back.dashboard');

        $response = $this
            ->actingAs($user, 'back')
            ->get($route);

        $response
            ->assertSeeText('Clients list');
    }

    /**
     * @return array
     */
    protected function generateUserData()
    {
        $data = factory(User::class)->raw();
        $data['password_confirmation'] = $data['password'];

        return $data;
    }

    /**
     * @return mixed
     */
    protected function generateClientData()
    {
        $data = factory(Client::class)->raw();

        return $data;
    }
}
